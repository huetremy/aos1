{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# AOS 1\n",
    "## Band reduction in multispectral images\n",
    "\n",
    "Authors : Mathilde Rineau, Rémy Huet\n",
    "\n",
    "A multispectral image is an image that has several components. For example, a color image\n",
    "has 3 components: red, green and blue and each pixel can be viewed as a vector in R3. More\n",
    "generally a multispectral image of size N×M with P spectral bands can be stored as a\n",
    "N×M×P array. There are N×M pixels living in Rp.\n",
    "\n",
    "\n",
    "When the number of spectral bands Pis too large, it is desirable to somehow reduce that\n",
    "number ultimately to 3 for viewing purposes. This process is called band reduction.\n",
    "\n",
    "The aim of this work is to propose a method using the PCA to perform a band reduction to 3 bands and to use it on a multispectral image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from scipy.io import loadmat\n",
    "\n",
    "# First load the image from the MATLAB data file\n",
    "image = loadmat('PaviaU.mat')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Introspection on `image` variable shows us its type and shape. The image is a python dictionary containing :\n",
    "\n",
    "- A header as a string\n",
    "- The version of the image\n",
    "- Some \"globals\" (empty array)\n",
    "- The image itself under `paviaU` as an array.\n",
    "\n",
    "We do not care about other data than the image, so we retrieve only the data in an `image_data` variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_data = image['paviaU']\n",
    "print(type(image_data))\n",
    "print(image_data.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The image is of type `ndarray`, a multidimensional array from `numpy`.\n",
    "\n",
    "The shape of the array is 610x340x103.\n",
    "This means that the image is composed of 610x340 \"pixels\" each composed of 103 bands.\n",
    "\n",
    "First, we resize the image as a two dimensional array : we keep the 103 bands but we \"merge\" the lines and columns as a single line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# X is our data reshaped as a vector of samples (each sample containing 103 bands)\n",
    "X = image_data.reshape((-1, 103))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For a good PCA analysis, the data needs to be rescaled.\n",
    "We will rescale it from 0 to 1 using `MinMaxScaler` from `scikit-learn`.\n",
    "\n",
    "We will perform our analysis with the value from 0 to 1.\n",
    "In a second time, we will rescale the data from 0 to 255 for printing purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import MinMaxScaler\n",
    "\n",
    "scaler = MinMaxScaler()\n",
    "scaler.fit(X)\n",
    "X_scaled = scaler.transform(X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can use PCA method on our data.\n",
    "\n",
    "For this purpose, we will use the `PCA` object of `scikit-learn`. This method centers the data so we don't need to do it ourselves.\n",
    "We will instantiate the object with a given number of components equal to 3 (we need 3 bands)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.decomposition import PCA\n",
    "\n",
    "pca = PCA(n_components=3)\n",
    "pca.fit(X_scaled)\n",
    "\n",
    "output_data = pca.transform(X_scaled)\n",
    "print(output_data.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The shape of our data is 207400x3.\n",
    "\n",
    "Using PCA, we kept the 208400 \"pixels\" but reduced band number to 3.\n",
    "\n",
    "The method `imshow` from `pyplot` accepts float data from 0 to 1 or integer data from 0 to 255.\n",
    "\n",
    "For simplicity, we will only rescale the PCA output from 0 to 1 and pass it to `imshow`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Re-fit the scaler for the output data\n",
    "scaler.fit(output_data)\n",
    "output_scaled = scaler.transform(output_data)\n",
    "\n",
    "# Reshape the output to print it as an image\n",
    "output_image = output_scaled.reshape((610, 340, 3))\n",
    "\n",
    "plt.imshow(output_image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Questions about the solution\n",
    "As requested, we have performed a dimension reduction to 3 principal components which is a very significant reduction considering that the original dimension was 103. We might have chosen another reduction for instance by considering the percentage of explained variance instead of the number of principal components or by drawing the associated scree plot.\n",
    "\n",
    "## Limits\n",
    "By reducing the dimension from 103 to 3 we have lost information and we can't know exactly what this information was and how much it was significant. However, this reduction allows us to visualize more easily the data, indeed band reduction is supposed to give the same color to similar objects.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploring variants of the methods\n",
    "\n",
    "As said before we might have choosen to use the explained variance to determine how much principal components have to be retained.\n",
    "You can see below two plots, the first one is a scree-plot which plots the importance of each principal components (from 1 to 103).\n",
    "The second one represents the cumulative explained variance in terms of number of principal components.\n",
    "We observe on the first plot that only 3 principal componants are really significant which is consistent to the results of the second plot where the explained variance grows very quickly to 1.\n",
    "Consequently, by using another implementation of PCA we would have had the same results. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.decomposition import PCA\n",
    "pca = PCA()\n",
    "pca.fit(X_scaled)\n",
    "plt.figure()\n",
    "plt.bar(range(1, X.shape[1]+1), pca.explained_variance_)\n",
    "plt.show()\n",
    "plt.figure()\n",
    "plt.plot(range(1, X.shape[1]+1), np.cumsum(pca.explained_variance_ratio_))\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "75384ff26d2d5ce1444ca190d7bd826ecec6b011430d85ce2059ae14ba6abb78"
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
